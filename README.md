A small set of Java utilities for marking fields as editable. An interface
for editing the values of the fields on specific objects at runtime can
then be automatically generated. A simple AWT dialog for editing such
properties using a text field is provided.